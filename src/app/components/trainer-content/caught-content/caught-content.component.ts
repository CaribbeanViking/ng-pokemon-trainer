import { Component, OnInit, Input } from '@angular/core';
import { Pokemons } from 'src/app/models/pokemon.model';
import { TrainerContentComponent } from '../trainer-content.component';
@Component({
  selector: 'app-caught-content',
  templateUrl: './caught-content.component.html',
  styleUrls: ['./caught-content.component.css'],
})
export class CaughtContentComponent implements OnInit {
  @Input() pokemon?: Pokemons;
  constructor(private trainerComponent: TrainerContentComponent) {}
  public onDelete(pokemon: Pokemons){
    this.trainerComponent.deletePokemon(pokemon)
  }
  ngOnInit(): void {}
}
