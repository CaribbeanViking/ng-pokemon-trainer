import { Component, OnDestroy, OnInit } from '@angular/core';
import { Pokemons } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';
import { CatchService } from 'src/app/services/catch.service';
@Component({
  selector: 'app-trainer-content',
  templateUrl: './trainer-content.component.html',
  styleUrls: ['./trainer-content.component.css'],
})
export class TrainerContentComponent{
  //current trainer pokemon displayed
  public trainer_pokemon: Pokemons[] | undefined 
  //length of the pokemon array
  public length : number | undefined
  constructor(
    private readonly userService : UserService, 
    private readonly catchService : CatchService) {

    this.trainer_pokemon = this.userService.user?.pokemon
    this.length = this.trainer_pokemon?.length
  }
  
  //If user exists in sessionStorage, remove pokemon from both session and API
  public deletePokemon(pokemon: Pokemons){
    
    if(this.userService.user){
      this.userService.removeFromUser(pokemon.name)
      this.catchService.patchTrainerApi(this.userService.user).subscribe({
        next: (trainer: any) => {
            this.userService.user = trainer
            this.trainer_pokemon = trainer.pokemon;
            this.length = trainer.pokemon.length
        },
        error: (error: any) => {
          console.log('ERROR', error.message);
        },
      });
    }
  }
}
