import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CatalogContentService } from 'src/app/services/catalog-content.service';

@Component({
  selector: 'app-catalog-content',
  templateUrl: './catalog-content.component.html',
  styleUrls: ['./catalog-content.component.css'],
})
export class CatalogContentComponent implements OnInit {
  get pokemons(): Pokemon {
    return this.pokemonCatalogService.pokemons;
  }

  get error(): string {
    return this.pokemonCatalogService.error;
  }

  constructor(private readonly pokemonCatalogService: CatalogContentService) {}
  ngOnInit(): void {
    this.pokemonCatalogService.getAllPokemons();  // Get pokemons for this instance
  }
}
