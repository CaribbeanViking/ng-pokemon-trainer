import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-add-item-content',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css'],
})
export class AddItemContentComponent {
  public isCaught: boolean = false;   // Bool to check if selected pokemon is caught or not
  @Input() pokemonCaught: string = ''; // Inject name of caught pokemon

  constructor(
    private readonly catchService: CatchService,
    private readonly userService: UserService
  ) {}

  ngOnInit(): void {
    this.isCaught = Boolean(this.userService.inCaught(this.pokemonCaught)); // On initialization get bool for selected Pokemon (check if it is in "inCaught")
  }

  onCatchClick(): void {
    if (this.userService.inCaught(this.pokemonCaught)) { // If Pokemon is already in Trainer Page...
      alert('You already have this Pokémon!');
    } else {
      this.catchService.catchPokemon(this.pokemonCaught) // Store Pokemon in this instance/user
      this.isCaught = this.userService.inCaught(this.pokemonCaught) // Set isCaught to true
    }
  }
}
