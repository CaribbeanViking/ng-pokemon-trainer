import { Component, OnInit, Input } from '@angular/core';
import { Pokemons } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-list-item-content',
  templateUrl: './list-item-content.component.html',
  styleUrls: ['./list-item-content.component.css'],
})
export class ListItemContentComponent implements OnInit {
  @Input() pokemon?: Pokemons; // Inject individual pokemon variable
  constructor() {}
  ngOnInit(): void {}
}
