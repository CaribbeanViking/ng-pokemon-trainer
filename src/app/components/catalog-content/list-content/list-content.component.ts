import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-list-content',
  templateUrl: './list-content.component.html',
  styleUrls: ['./list-content.component.css'],
})
export class ListContentComponent implements OnInit {
  @Input() pokemons: Pokemon = {  // Inject Pokemon class
    count: 0,
    next: '',
    previous: null,
    results: [],
  };
  constructor() {}
  ngOnInit(): void {}
}
