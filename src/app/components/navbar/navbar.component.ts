import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent{
  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) { }

  //set user to undefined so a new user can log in
  public logOut(){
    this.userService.user = undefined
  }
 
}
