import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { Trainer } from "src/app/models/trainer.model";
import { LoginService } from "src/app/services/login.service";
import { UserService } from "src/app/services/user.service";
@Component({
    selector:'app-login-component',
    templateUrl: './trainer-login.component.html',
    styleUrls: ['./trainer-login.component.css']
})
//Login class
export class TrainerLoginComponent implements OnInit{
    @Output() login: EventEmitter<void> = new EventEmitter()

    constructor(
        private readonly loginService: LoginService,
        private readonly userService: UserService,
        private readonly router: Router
    ){}
    ngOnInit(): void {
        //User already logged in? go back to catalog page
        if(this.userService.user){
            this.router.navigateByUrl("/catalog")
        }
    }
    //uses the login service and if successful redirects user to catalog page
    public loginSubmit(form: NgForm): void {
        
        const { username } = form.value
        this.loginService.login(username)
            .subscribe({
                next: (user: Trainer) => {
                    this.userService.user = user
                    this.login.emit()
                    this.router.navigateByUrl("/catalog")
                },
                error: () => {
                    //handle errors
                }
            })

    }
}