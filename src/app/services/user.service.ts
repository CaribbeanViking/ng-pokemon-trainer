import { Injectable } from '@angular/core';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';
import { Pokemons } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _trainer?: Trainer; // Set Trainer object

  get user(): Trainer | undefined {
    return this._trainer;
  }
  set user(user: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>('user', user!); // Store user in session
    this._trainer = user;
  }

  public removeFromUser(pokemonName: string) {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter( // Get all pokemons that are not argument
        (pokemon: Pokemons) => pokemon.name != pokemonName
      );
    }
  }
  constructor() {
    this._trainer = StorageUtil.storageRead('user');
  }

  // Method that checks if Pokemon is already caught (True if yes)
  public inCaught(pokemonName: string): boolean {
    if (this._trainer) {
      for (let pokemon of this._trainer.pokemon) { // Loop all Pokemons
        if (pokemon.name === pokemonName) { // Compare names
          return true;
        }
      }
    }
    return false;
  }
}
