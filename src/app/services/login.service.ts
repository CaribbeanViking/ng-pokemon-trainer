import { Injectable } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
const url = environment.api_trainers;
@Injectable({
  providedIn: 'root',
})
export class LoginService {

    //dependency Injection
    constructor(private readonly http: HttpClient){

    }
    //check if trainer already exists
    private checkTrainer(username: string): Observable<Trainer | undefined>{
     
        return this.http.get<Trainer[]>(`${url}?username=${username}`)
            .pipe(
                map((response: Trainer[]) => response.pop())
                    
            )
            
       
    }
    //create a new trainer
    private createTrainer(username: string): Observable<Trainer>{
        const trainer = {
            username: username,
            pokemon: [],
        }
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'X-API-Key': environment.api_key_trainers 
        })
        return this.http.post<Trainer>(url, trainer, {
            headers: headers
        })
    }
    //first checks whether trainer exists, if it does not, create new trainer.
    public login(username: string): Observable<Trainer>{
        return this.checkTrainer(username)
        .pipe(switchMap((user: Trainer | undefined) => {
          if(user === undefined){
            return this.createTrainer(username)
          }
          return of(user)
        }))
            
    }
}
