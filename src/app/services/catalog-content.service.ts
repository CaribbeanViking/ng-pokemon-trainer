import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Pokemon, Pokemons } from '../models/pokemon.model';
const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root',
})
export class CatalogContentService {
  private _pokemons: Pokemon = { // Initialize Pokemon collection object
    count: 0,
    next: '',
    previous: null,
    results: [],
  };

  private _error: string = ''; // Initialize error msg

  get pokemons(): Pokemon {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  constructor(private readonly http: HttpClient) {}

  public getAllPokemons(): void { // Get all Pokemons from PokeAPI
    this.http.get<Pokemon>(apiPokemons).subscribe({
      next: (pokemons: Pokemon) => {
        this._pokemons = pokemons;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      },
    });
  }

  public pokemonByName(name: string): Pokemons | undefined { // Method to look for specific Pokemon
    return this._pokemons.results.find(
      (pokemon: Pokemons) => pokemon.name === name && pokemon.url === pokemon.url // Return if name and url match from Pokemon.results
    );
  }
}
