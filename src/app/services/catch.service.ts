import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { CatalogContentService } from './catalog-content.service';
import { Pokemons } from '../models/pokemon.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

@Injectable({
  providedIn: 'root',
})
export class CatchService {
  constructor(
    private http: HttpClient,
    private readonly catalogService: CatalogContentService,
    private readonly userService: UserService
  ) {}
  public patchTrainerApi(trainer: Trainer): Observable<any> {
    // Method to patch the API used for Trainer Page
    const headers = {
      headers: new HttpHeaders({
        'content-type': 'application/json',
        'x-api-key': environment.api_key_trainers,
      }),
    };
    return this.http.patch(
      `${environment.api_trainers}/${trainer.id}`,
      JSON.stringify(trainer),
      headers
    );
  }

  public catchPokemon(pokemonName: string) {
    if (!this.userService.user) {
      // If there is no Trainer..
      throw new Error('There is no Trainer available.');
    }

    const newPokemon: Pokemons | undefined = // Search for new Pokemon to catch
      this.catalogService.pokemonByName(pokemonName);

    if (!newPokemon) {
      // If there is none..
      throw new Error('No pokemon with that name!');
    }

    this.userService.user.pokemon.push(newPokemon); // Push caught Pokemon to user

    this.patchTrainerApi(this.userService.user).subscribe({
      // Patch Trainer API
      next: (response: any) => {
        this.userService.user = response; // Update user with API response
      },

      error: (error: any) => {
        console.log('ERROR', error.message);
      },
    });
  }
}
