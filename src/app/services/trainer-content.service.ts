import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon, Pokemons } from '../models/pokemon.model';
const { api_trainers } = environment;

@Injectable({
  providedIn: 'root',
})
export class TrainerContentService {
  private _pokemons: Pokemon = {
    count: 0,
    next: '',
    previous: null,
    results: [],
  };

  private _error: string = '';

  get pokemons(): Pokemon {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  constructor(private readonly http: HttpClient) {}

  public getAllPokemons(): void {
    this.http.get<Pokemon>(api_trainers).subscribe({
      next: (pokemons: Pokemon) => {
        this._pokemons = pokemons;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      },
    });
  }

/*   public pokemonByName(name: string): Pokemons | undefined {
    return this._pokemons.results.find(
      (pokemon: Pokemons) => pokemon.name === name
    );
  } */
}
