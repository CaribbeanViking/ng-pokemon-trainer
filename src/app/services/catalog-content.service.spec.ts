import { TestBed } from '@angular/core/testing';

import { CatalogContentService } from './catalog-content.service';

describe('CatalogContentService', () => {
  let service: CatalogContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatalogContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
