import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CatalogContentComponent } from "./components/catalog-content/catalog-content.component";
import { HomeContentComponent } from "./components/home-content/home-content.component";
import { TrainerContentComponent } from "./components/trainer-content/trainer-content.component";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/login"
    },
    {
        path: "catalog",
        component: CatalogContentComponent,
        canActivate: [AuthGuard]
    },
    {
        path: "trainer",
        component: TrainerContentComponent,
        canActivate: [AuthGuard]
    },
    {
        path: "login",
        component: HomeContentComponent
    }

]

@NgModule({
    imports: [RouterModule.forRoot(routes)], //Import module
    exports: [RouterModule]  //Expose module and its features
})
export class AppRoutingModule{

}    