import { Pokemons } from "./pokemon.model";

export interface Trainer {  // Trainer (User) object
  id: number;
  username: string;
  pokemon: Pokemons[];
}
