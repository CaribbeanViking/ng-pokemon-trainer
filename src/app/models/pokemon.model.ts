export interface Pokemon { // Pokemon collection object
    count: number,
    next: string,
    previous: null,
    results: Pokemons[]
}

export interface Pokemons { // Individual Pokemon object
    name: string,
    url: string
}