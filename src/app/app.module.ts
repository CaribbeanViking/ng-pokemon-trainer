import { NgModule } from '@angular/core';

//MODULES//
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

//COMPONENTS//
import { AppComponent } from './app.component';
import { CatalogContentComponent } from './components/catalog-content/catalog-content.component';
import { ListContentComponent } from './components/catalog-content/list-content/list-content.component';
import { HomeContentComponent } from './components/home-content/home-content.component';
import { TrainerContentComponent } from './components/trainer-content/trainer-content.component';
import { ListItemContentComponent } from './components/catalog-content/list-content/list-item-content/list-item-content.component';
import { TrainerLoginComponent } from './components/home-content/trainer-login-content/trainer-login.component';
import { FormsModule } from '@angular/forms';
import { AddItemContentComponent } from './components/catalog-content/list-content/list-item-content/add-item-content/add-item.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CaughtContentComponent } from './components/trainer-content/caught-content/caught-content.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeContentComponent,
    TrainerContentComponent,
    CatalogContentComponent,
    ListContentComponent,
    ListItemContentComponent,
    AddItemContentComponent,
    TrainerLoginComponent,
    NavbarComponent,
    CaughtContentComponent,
  ],
  imports: [BrowserModule, 
    AppRoutingModule, 
    HttpClientModule, 
    FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
