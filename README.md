## Release
Version: 1.0
Date: 2022-07-25

## Description
This is an application which allows a user (Trainer) to login/create a new user and access a catalog of Pokemon-cards. The user may then select cards which will stored in "caught-pokemons" on a Trainer-page. The user can freely add and remove the cards on these pages. The project is a 

The project is generated using [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6.

## Installation

The project uses npm and Node.js.

## Using the application

Run "ng serve" and go to "http://localhost:4200/".

Login Page: Enter user (trainer) name to login. If you have logged in with the same name before, it will be stored.

Catalog Page: The catalog lists 36 pokemons with name and avatar. The bottom right button on each pokemon is used to "catch" a Pokemon. The button also indicates if the Pokemon is already caught.

Trainer Page: Here, stored Pokemons will be shown. The bottom right button will remove the Pokemon from the Trainer page.

## Help

Type "ng help" for more info about Angular.

## Contributors

https://gitlab.com/CaribbeanViking

https://gitlab.com/saintgod

Both Experis Academy employees enrolled in the JS/C# program.

## Contributing

No one else than above mentioned.

## Known Bugs

Application is not optimally responsive.